
import java.util.*;
public class Main
{
	public static void main(String[] args) 
	{
	    
	    
	    Scanner reader = new Scanner(System.in);
	    
	    
        ///Enter the number
        System.out.print("Enter a number: ");
        
        
        int num = reader.nextInt();
        
        
        //Checking the logic
        if(num % 2 == 0)
        
            System.out.println(num + " is even");
            
        else
        
            System.out.println(num + " is odd");
	    
	}
}
